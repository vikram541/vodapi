<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Network\Response;
use Cake\Network\Http\Message;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Flash');
    }

    public function isValidRequest($postdata) {
//      
        $hash_value = $postdata['hash_value'];
        $client_id = $postdata['client_id'];
        unset($postdata["hash_value"]);
        $data = "";
        foreach ($postdata as $k => $v) {
            if ($data == "") {
                $data = $v;
            } else {
                $data.="|" . $v;
            }
        }
        $this->loadModel('Applications');
        $db_secrate_key = $this->Applications->find('all')->where(['client_id' => $client_id]);
        $seckey="";
        foreach($db_secrate_key as $v){
            $seckey=$v['secrate_key'];
        }
        $new_hash_value = hash_hmac("sha1", $data, $seckey);
        if ($hash_value == $new_hash_value) {
            return true;
        } else {
            return false;
        }
    }

    public function invalidoperation(){
        $message = "Invalid operation";
        $this->set(['code'=>'404','msgstatus'=>Configure::read('status.404'),
                    'message' => $message,
                    '_serialize' => ['message','msgstatus','code']
                  ]);
    }
}
