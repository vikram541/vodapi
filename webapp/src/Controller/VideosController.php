<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Videos Controller
 *
 * @property \App\Model\Table\VideosTable $Videos
 */
class VideosController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->set('videos', $this->paginate($this->Videos));
        $this->set('_serialize', ['videos']);
    }

    public function videoData() {
        $response = array();
        if ($this->request->is('post')) {
            if ($this->isValidRequest($this->request->data)) {
                if ($data = $this->Videos->find('all')->where(['id' => $this->request->data['id']])) {
                    $d1 = "";
                    foreach ($data as $row) {
                        $d1 = $row;
                    }
                    $response['code'] = "200";
                    $response['data'] = $d1;
                } else {
                    //$this->Flash->error(__('The video could not be saved. Please, try again.'));
                    $response['code'] = "406";
                    $response['msg'] = "Error in Get Video Data!";
                    $response['errors'] = $video->errors();
                    $response['data'] = $this->request->post;
                }
            } else {
                $response['code'] = "401";
                $response['msg'] = "Unauthorized Request!";
            }
        } else {
            $response['code'] = "400";
            $response['msg'] = "Bad Request!";
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
     * View method
     *
     * @param string|null $id Video id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view() {
        $response = array();
        if ($this->request->is('post')) {
            if ($this->isValidRequest($this->request->data)) {
                if ($data = $this->Videos->find('all')->where(['id' => $this->request->data['id']])) {
                    $d1 = "";
                    foreach ($data as $row) {
                        $d1 = $row;
                    }
                    $response['code'] = "200";
                    $response['data'] = $d1;
                } else {
                    //$this->Flash->error(__('The video could not be saved. Please, try again.'));
                    $response['code'] = "406";
                    $response['msg'] = "Error in Get Video Data!";
                    $response['errors'] = $video->errors();
                    $response['data'] = $this->request->post;
                }
            } else {
                $response['code'] = "401";
                $response['msg'] = "Unauthorized Request!";
            }
        } else {
            $response['code'] = "400";
            $response['msg'] = "Bad Request!";
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $response = array();
        $video = $this->Videos->newEntity();
        if ($this->request->is('post')) {
            if ($this->isValidRequest($this->request->data)) {
                $video = $this->Videos->patchEntity($video, $this->request->data, ['validate' => false]);
                if ($id = $this->Videos->save($video)) {
                    $response['code'] = "200";
                    $response['msg'] = "Video Url Saved";
                    $response['id'] = $id['id'];
                    $response['url'] = $this->request->data['video_link'];
                } else {
                    //$this->Flash->error(__('The video could not be saved. Please, try again.'));
                    $response['code'] = "406";
                    $response['msg'] = "Error in Saving Video Data!";
                    $response['errors'] = $video->errors();
                    $response['data'] = $this->request->post;
                }
            } else {
                $response['code'] = "401";
                $response['msg'] = "Unauthorized Request!";
            }
        } else {
            $response['code'] = "400";
            $response['msg'] = "Bad Request!";
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Video id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $response = array();
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->isValidRequest($this->request->data)) {
//                echo "<pre>";
//                print_r($this->request->data);
                $video = $this->Videos->get($this->request->data['id'], [
                    'contain' => []
                ]);
                $video = $this->Videos->patchEntity($video, $this->request->data);
                if ($id = $this->Videos->save($video)) {
                    $response['code'] = "200";
                    $response['msg'] = "Video Data Updated";
                    $response['id'] = $id['id'];
                } else {
                    //$this->Flash->error(__('The video could not be saved. Please, try again.'));
                    $response['code'] = "406";
                    $response['msg'] = "Error in Saving Video Data!";
                    $response['errors'] = $video->errors();
                    $response['data'] = $this->request->data;
                }
            } else {
                $response['code'] = "401";
                $response['msg'] = "Unauthorized Request!";
            }
        } else {
            $response['code'] = "400";
            $response['msg'] = "Bad Request!";
        }

        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Video id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $video = $this->Videos->get($id);
        if ($this->Videos->delete($video)) {
            $this->Flash->success(__('The video has been deleted.'));
        } else {
            $this->Flash->error(__('The video could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
