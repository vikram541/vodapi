<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Http\Message;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    public function index()
    {
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }
    public function signup()
    {
      

      if ($this->request->is('post')) {
             $userdata = $this->Users->newEntity($this->request->data);  
            if ($this->isValidRequest($this->request->data)) {
                unset($this->request->data['client_id']);
                unset($this->request->data['hash_value']);
                if ($this->Users->save($userdata)) {
                    $message = 'Success';
                     $this->set([
                    'code'=>'200',    
                    'msgstatus'=>Configure::read('status.200'),    
                    'message' => $message,
                    'userdata' => $userdata,
                    '_serialize' => ['message','userdata','msgstatus','code']
                ]);
                } else {
                  $errmessage="";
                    foreach ($userdata->errors() as $userkey => $value) {
                       $errmessage[$userkey]=$value;
                    }
                    $message = 'Error';
                    $this->set([
                    'code'=>'406',
                    'msgstatus'=>Configure::read('status.406'),    
                    'message' => $message,  
                    'userdata' => $userdata,
                    'errormessage'=>$errmessage,
                    '_serialize' => ['message','errormessage','userdata','msgstatus','code']
                ]);
                }
             }else{
                $this->invalidoperation();

             }   
        }    
    }

      public function login() {
         if ($this->request->is('post')) {
            $status=""; 
            if ($this->isValidRequest($this->request->data)) {
               unset($this->request->data['client_id']);
               unset($this->request->data['hash_value']);
               unset($this->request->data['remember']);
               $userdata=$this->request->data;  
               $get_user_data=$this->Users->findByUsernameAndPassword($userdata['username'],sha1($userdata['password']));
               $count=0;
                foreach ($get_user_data as $uservalue) {
                    $uid=$uservalue->get('id');
                    $firstname=$uservalue->get('firstname');
                    $lastname=$uservalue->get('lastname');
                    $username=$uservalue->get('username');
                    $email=$uservalue->get('email');  
                    $role=$uservalue->get('role'); 
                    $userdata=array('id'=>$uid,'firstname'=>$firstname,'lastname'=>$lastname,'username'=>$username,'email'=>$email,'role'=>$role);                  
                    $count++;
                }   
                if($count==1){
                $message = "Success";
                $this->set([
                    'code'=>'200',    
                    'msgstatus'=>Configure::read('status.200'),    
                    'message' => $message,
                    'userdata' => $userdata,
                    '_serialize' => ['message','userdata','msgstatus','code']]);      

                }else{
                   $message = 'Error';
                    $this->set([
                    'code'=>'406',
                    'msgstatus'=>Configure::read('status.406'),    
                    'message' => $message,  
                    '_serialize' => ['message','msgstatus','code']]);
                }
              }else{
               $this->invalidoperation();
              }  
           
           }  

      }
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // public function view($id = null)
    // {
    //     $user = $this->Users->get($id, [
    //         'contain' => []
    //     ]);
    //     $this->set('user', $user);
    //     $this->set('_serialize', ['user']);
    // }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $user = $this->Users->newEntity();
    //     if ($this->request->is('post')) {
    //         $user = $this->Users->patchEntity($user, $this->request->data);
    //         if ($this->Users->save($user)) {
    //             $this->Flash->success(__('The user has been saved.'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('The user could not be saved. Please, try again.'));
    //         }
    //     }
    //     $this->set(compact('user'));
    //     $this->set('_serialize', ['user']);
    // }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // public function edit($id = null)
    // {
    //     $user = $this->Users->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $user = $this->Users->patchEntity($user, $this->request->data);
    //         if ($this->Users->save($user)) {
    //             $this->Flash->success(__('The user has been saved.'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('The user could not be saved. Please, try again.'));
    //         }
    //     }
    //     $this->set(compact('user'));
    //     $this->set('_serialize', ['user']);
    // }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $user = $this->Users->get($id);
    //     if ($this->Users->delete($user)) {
    //         $this->Flash->success(__('The user has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The user could not be deleted. Please, try again.'));
    //     }
    //     return $this->redirect(['action' => 'index']);
    // }
}
