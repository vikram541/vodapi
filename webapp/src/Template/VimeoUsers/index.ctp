<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Vimeo User'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="vimeoUsers index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('cid') ?></th>
            <th><?= $this->Paginator->sort('client_secret') ?></th>
            <th><?= $this->Paginator->sort('client_access_token') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($vimeoUsers as $vimeoUser): ?>
        <tr>
            <td><?= $this->Number->format($vimeoUser->id) ?></td>
            <td><?= h($vimeoUser->cid) ?></td>
            <td><?= h($vimeoUser->client_secret) ?></td>
            <td><?= h($vimeoUser->client_access_token) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $vimeoUser->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $vimeoUser->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $vimeoUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vimeoUser->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
