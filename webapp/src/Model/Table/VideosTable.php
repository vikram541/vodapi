<?php
namespace App\Model\Table;

use App\Model\Entity\Video;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Videos Model
 *
 */
class VideosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('videos');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('userid', 'valid', ['rule' => 'numeric'])
            ->requirePresence('userid', 'create')
            ->notEmpty('userid');
            
        $validator
            ->requirePresence('genres', 'create')
            ->notEmpty('genres');
            
        $validator
            ->requirePresence('topics', 'create')
            ->notEmpty('topics');
            
        $validator
            ->requirePresence('tags', 'create')
            ->notEmpty('tags');
            
        $validator
            ->add('countryid', 'valid', ['rule' => 'numeric'])
            ->requirePresence('countryid', 'create')
            ->notEmpty('countryid');
            
        $validator
            ->requirePresence('video_title', 'create')
            ->notEmpty('video_title');
            
        $validator
            ->requirePresence('video_year', 'create')
            ->notEmpty('video_year');
            
        $validator
            ->requirePresence('video_description', 'create')
            ->notEmpty('video_description');
            
        $validator
            ->requirePresence('video_director', 'create')
            ->notEmpty('video_director');
            
        $validator
            ->requirePresence('video_synopsis', 'create')
            ->notEmpty('video_synopsis');
            
        $validator
            ->requirePresence('country', 'create')
            ->notEmpty('country');
            
        $validator
            ->requirePresence('film_festival', 'create')
            ->notEmpty('film_festival');
            
        $validator
            ->requirePresence('language', 'create')
            ->notEmpty('language');
            
        $validator
            ->requirePresence('video_logline', 'create')
            ->notEmpty('video_logline');
            
        $validator
            ->requirePresence('video_cast', 'create')
            ->notEmpty('video_cast');
            
        $validator
            ->requirePresence('video_link', 'create')
            ->notEmpty('video_link')
            ->add('video_link', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
            
        $validator
            ->add('video_length', 'valid', ['rule' => 'numeric'])
            ->requirePresence('video_length', 'create')
            ->notEmpty('video_length');
            
        $validator
            ->add('video_size', 'valid', ['rule' => 'numeric'])
            ->requirePresence('video_size', 'create')
            ->notEmpty('video_size');
            
        $validator
            ->requirePresence('video_file_type', 'create')
            ->notEmpty('video_file_type');
            
        $validator
            ->requirePresence('channel', 'create')
            ->notEmpty('channel');
            
        $validator
            ->requirePresence('video_trailer_url', 'create')
            ->notEmpty('video_trailer_url');
            
        $validator
            ->requirePresence('video_twitter', 'create')
            ->notEmpty('video_twitter');
            
        $validator
            ->requirePresence('video_fbpage', 'create')
            ->notEmpty('video_fbpage');
            
        $validator
            ->allowEmpty('video_referal');
            
        $validator
            ->add('total_like', 'valid', ['rule' => 'numeric'])
            ->requirePresence('total_like', 'create')
            ->notEmpty('total_like');
            
        $validator
            ->add('total_share', 'valid', ['rule' => 'numeric'])
            ->requirePresence('total_share', 'create')
            ->notEmpty('total_share');
            
        $validator
            ->add('is_hd_sd', 'valid', ['rule' => 'boolean'])
            ->requirePresence('is_hd_sd', 'create')
            ->notEmpty('is_hd_sd');
            
        $validator
            ->add('is_paid', 'valid', ['rule' => 'boolean'])
            ->requirePresence('is_paid', 'create')
            ->notEmpty('is_paid');
            
        $validator
            ->add('price', 'valid', ['rule' => 'numeric'])
            ->requirePresence('price', 'create')
            ->notEmpty('price');
            
        $validator
            ->add('is_approved', 'valid', ['rule' => 'boolean'])
            ->requirePresence('is_approved', 'create')
            ->notEmpty('is_approved');
            
        $validator
            ->add('need_signup', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('need_signup');
            
//        $validator
//            ->add('modidfied', 'valid', ['rule' => 'date'])
//            ->requirePresence('modidfied', 'create')
//            ->notEmpty('modidfied');

        return $validator;
    }
    
    
}
